#include <QApplication>

#include "audioengine.h"
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    AudioEngine::Instance()->init();

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
