#ifndef OSCILLATORWIDGET_H
#define OSCILLATORWIDGET_H

#include <QWidget>
#include "oscillatormodule.h"

namespace Ui {
class OscillatorWidget;
}

class OscillatorWidget : public QWidget
{
    Q_OBJECT

public:
    explicit OscillatorWidget(QWidget *parent = 0, OscillatorModule *module = NULL);
    ~OscillatorWidget();

    void Init(OscillatorModule *module);

private slots:
    void on_dialOctave_valueChanged(int value);

    void on_dialFine_valueChanged(int value);

    void on_sliderSine_valueChanged(int value);

    void on_sliderSaw_valueChanged(int value);

    void on_sliderSqr_valueChanged(int value);

private:
    Ui::OscillatorWidget *ui;
    OscillatorModule *_module;
};

#endif // OSCILLATORWIDGET_H
