#include "oscillatormodule.h"

#include <stdio.h>

OscillatorModule::OscillatorModule()
{
    _octave = 5;
    _note = 0;
    _fine = 0;

    _sineVol = 1.0;
    _sawVol = 0.0;
    _squareVol = 0.0;

    calcFreq();

    _sine = new SineWave();
    _saw = new BlitSaw();
    _square = new BlitSquare();
}

void OscillatorModule::calcFreq() {
    //http://newt.phys.unsw.edu.au/jw/notes.html
    int n = _octave*12+_note;
    _freq = pow(2.0,((n-60)/12))*261.6+_fine;

    //if 0 is A
    //_freq = pow(2.0,((n-69)/12))*440;
}


StkFloat OscillatorModule::output() {
    _sine->setFrequency(_freq);
    _saw->setFrequency(_freq);
    _square->setFrequency(_freq);

    /*
     * https://dsp.stackexchange.com/questions/3581/algorithms-to-mix-audio-signals-without-clipping
     * the best way to mix signals without clipping them seems to be this formula:
     * output = (sig1+sig2+sig3)/numberOfSignals
    */
    return (_sine->tick()*_sineVol+_saw->tick()*_sawVol+_square->tick()*_squareVol)/3;
}

void OscillatorModule::input(StkFloat signal) {

}

void OscillatorModule::SetOctave(int octave) {
    _octave = octave;
    calcFreq();
}

void OscillatorModule::SetFine(int cc) {
    _fine = cc/128.0*96.0;
    calcFreq();
}


void OscillatorModule::SetVolumes(int ccSine, int ccSaw, int ccSquare) {
    if (ccSine>-1) {
        _sineVol = ccSine/128.0;
    }

    if (ccSaw>-1) {
        _sawVol = ccSaw/128.0;
    }

    if (ccSquare>-1) {
        _squareVol = ccSquare/128.0;
    }
}
