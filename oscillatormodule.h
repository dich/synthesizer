#ifndef OSCILLATORMODULE_H
#define OSCILLATORMODULE_H

#include "SineWave.h"
#include "BlitSaw.h"
#include "BlitSquare.h"

#include <cmath>

using namespace stk;

class OscillatorModule
{
private:
    SineWave *_sine;
    BlitSaw *_saw;
    BlitSquare *_square;

    int _octave;
    int _note;

    StkFloat _freq;
    StkFloat _fine;

    StkFloat _sineVol;
    StkFloat _sawVol;
    StkFloat _squareVol;

    void calcFreq();
public:
    OscillatorModule();
    StkFloat output();
    void input(StkFloat signal);

    void SetOctave(int octave);
    void SetFine(int cc);
    void SetVolumes(int ccSine, int ccSaw, int ccSquare);
};

#endif // OSCILLATORMODULE_H
