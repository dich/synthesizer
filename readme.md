# Synthesizer Project

Synthesizer is a monophonic 2 oscillators semi-modular stand alone synthesizer (surprise, surprise!)
This is my first project on audio developement. 
It is developed with C++ using QT and STK and the main platform right now is Linux Ubuntu.

## Current version

![alt tag](https://dl.dropboxusercontent.com/u/16635236/synthesizer2.png)

Two working oscillators with a better mixer, both for waves and oscillators. 

## History

![alt tag](https://dl.dropboxusercontent.com/u/16635236/synthesizer.png)

This is a learning/work in progress project, right now the first oscillator is working allowing to change its parameters: octave, fine tuning and volume of the three waveform (sine, saw and square).
The mixer sucks and the next step is to find a proper way to mix several sound source.

## Initial setup for QT

Create a new QT Widgets application named "Synthesizer"

## linux ubuntu setup

1. install stk

```
sudo apt-get install libstk0-dev
```

2. add stk to the Synthesizer.pro file

```
INCLUDEPATH += /usr/include/stk
LIBS += -L/usr/lib -lstk -lrtaudio
```

## repo setup

```
git init
git remote add origin git@bitbucket.org:dich/synthesizer.git
git pull
```