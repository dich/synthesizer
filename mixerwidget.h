#ifndef MIXERWIDGET_H
#define MIXERWIDGET_H

#include <QWidget>
#include "audioengine.h"

namespace Ui {
class MixerWidget;
}

class MixerWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MixerWidget(QWidget *parent = 0);
    ~MixerWidget();

private slots:
    void on_dialOsc1_valueChanged(int value);

    void on_dialOsc2_valueChanged(int value);

private:
    Ui::MixerWidget *ui;
};

#endif // MIXERWIDGET_H
