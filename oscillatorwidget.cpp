#include "oscillatorwidget.h"
#include "ui_oscillatorwidget.h"

OscillatorWidget::OscillatorWidget(QWidget *parent, OscillatorModule *module) :
    QWidget(parent),
    ui(new Ui::OscillatorWidget)
{
    this->Init(module);
    ui->setupUi(this);
}

OscillatorWidget::~OscillatorWidget()
{
    delete ui;
}

void OscillatorWidget::Init(OscillatorModule *module) {
    _module = module;
}

void OscillatorWidget::on_dialOctave_valueChanged(int value)
{
    _module->SetOctave(value);
}

void OscillatorWidget::on_dialFine_valueChanged(int value)
{
    _module->SetFine(value);
}

void OscillatorWidget::on_sliderSine_valueChanged(int value)
{
    _module->SetVolumes(value,-1,-1);
}

void OscillatorWidget::on_sliderSaw_valueChanged(int value)
{
    _module->SetVolumes(-1,value,-1);
}

void OscillatorWidget::on_sliderSqr_valueChanged(int value)
{
    _module->SetVolumes(-1,-1,value);
}
