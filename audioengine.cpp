#include "audioengine.h"

#include <stdio.h>

//singleton
AudioEngine* AudioEngine::_instance = NULL;

AudioEngine* AudioEngine::Instance() {
    if(_instance == NULL){
        _instance = new AudioEngine();
    }
    return _instance;
}

AudioEngine::AudioEngine() {

}

AudioEngine::~AudioEngine() {
    if(_instance != 0) delete _instance;
}
//end singleton

int AudioEngine::init() {
    Stk::setSampleRate( 44100.0 );

    // Figure out how many bytes in an StkFloat and setup the RtAudio stream.
    RtAudio::StreamParameters parameters;
    parameters.deviceId = 6;//dac.getDefaultOutputDevice();

    parameters.nChannels = 2;

    format = ( sizeof(StkFloat) == 8 ) ? RTAUDIO_FLOAT64 : RTAUDIO_FLOAT32;
    bufferFrames = RT_BUFFER_SIZE;// * 2;

    printf("%u\n",parameters.deviceId);


    for (int i=0; i<dac.getDeviceCount(); i++) {
        printf("DEVICE %u NAME: %s\n",i,dac.getDeviceInfo(i).name.c_str());
    }

    try {
        dac.openStream(&parameters, NULL, format, (unsigned int)Stk::sampleRate(), &bufferFrames, &tick, NULL);
    } catch (RtError &error) {
        error.printMessage();
        return 0;
    }

    _osc1 = new OscillatorModule();
    _osc2 = new OscillatorModule();

    _osc1Vol = 0;
    _osc2Vol = 0;

    try {
        dac.startStream();
    }
    catch (RtError &error) {
        error.printMessage();
        return 0;
    }

    return 1;
}

StkFloat AudioEngine::tick() {
    return (_osc1->output()*_osc1Vol+_osc2->output()*_osc2Vol)/2;
}

int AudioEngine::tick(void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
         double streamTime, RtAudioStreamStatus status, void *dataPointer )
{
    register StkFloat *samples = (StkFloat *) outputBuffer;

    for ( unsigned int i=0; i<nBufferFrames; i++ ) {
        StkFloat f = AudioEngine::Instance()->tick();
        AudioEngine::Instance()->SampledThick[i] = f;

        *samples++ = f;
        //if there are two channels, you need to write two samples each time:
        *samples++ = f;
    }

    return 0;
}

OscillatorModule *AudioEngine::GetOscillatorModule1() {
    return _osc1;
}

OscillatorModule *AudioEngine::GetOscillatorModule2() {
    return _osc2;
}

void AudioEngine::SetOscillatorsVolume(int cc1, int cc2) {
    if (cc1>-1) {
        _osc1Vol = cc1/128.0;
    }

    if (cc2>-1) {
        _osc2Vol = cc2/128.0;
    }
}
