#ifndef AUDIOENGINE_H
#define AUDIOENGINE_H

#include "SineWave.h"
#include "RtAudio.h"
#include "RtError.h"

#include "oscillatormodule.h"

using namespace stk;

class AudioEngine
{
private:
    //singleton stuffs
    static AudioEngine* _instance;
    AudioEngine();
    ~AudioEngine();
    AudioEngine(const AudioEngine&); // Prevent copy-construction
    AudioEngine& operator=(const AudioEngine&);	// Prevent assignment
    //end singleton stuffs

    RtAudio dac;
    //SineWave sine;
    SineWave sub;
    RtAudioFormat format;
    unsigned int bufferFrames;

    OscillatorModule *_osc1;
    OscillatorModule *_osc2;

    StkFloat _osc1Vol;
    StkFloat _osc2Vol;

    static int tick(void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
             double streamTime, RtAudioStreamStatus status, void *dataPointer );
public:
    //singleton stuffs
    static AudioEngine *Instance();
    //end singleton stuffs

    int init();
    StkFloat tick();

    OscillatorModule *GetOscillatorModule1();
    OscillatorModule *GetOscillatorModule2();

    void SetOscillatorsVolume(int cc1, int cc2);

    float SampledThick[RT_BUFFER_SIZE];
    int SampledThickLength = RT_BUFFER_SIZE;
};

#endif // AUDIOENGINE_H
