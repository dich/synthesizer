#include "mixerwidget.h"
#include "ui_mixerwidget.h"

MixerWidget::MixerWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MixerWidget)
{
    ui->setupUi(this);
}

MixerWidget::~MixerWidget()
{
    delete ui;
}

void MixerWidget::on_dialOsc1_valueChanged(int value)
{
    AudioEngine::Instance()->SetOscillatorsVolume(value,-1);
}

void MixerWidget::on_dialOsc2_valueChanged(int value)
{
    AudioEngine::Instance()->SetOscillatorsVolume(-1,value);
}
