#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <stdio.h>
#include <math.h>

#define PI 3.14159265

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    OscillatorWidget *ow1 = new OscillatorWidget(0,AudioEngine::Instance()->GetOscillatorModule1());
    OscillatorWidget *ow2 = new OscillatorWidget(0,AudioEngine::Instance()->GetOscillatorModule2());

    ui->mainFormLayout->addWidget(ow1);
    ui->mainFormLayout->addWidget(ow2);
    ui->mainFormLayout->addWidget(new MixerWidget());

    _scene = new QGraphicsScene(this);
    ui->gvOscilloscope->setScene(_scene);
    //GraphicsViewEventHandler *gveh = new GraphicsViewEventHandler(ui->gvTester);

    _timer.start();
    timerId = startTimer(500);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked(bool checked)
{
    /*OscillatorWidget *ow1 = new OscillatorWidget(0,AudioEngine::Instance()->GetOscillatorModule1());
    ui->mainFormLayout->addWidget(ow1);*/
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    //_timer.elapsed();

    int width = ui->gvOscilloscope->width();
    int height = ui->gvOscilloscope->height();

    _scene->clear();

    QBrush brush(QColor(33,33,33));
    QPen pen(QColor(128,128,128));

    QBrush brush2(QColor(0,128,0));
    QPen pen2(QColor(0,128,0));

    pen.setWidth(1);
    pen2.setWidth(2);

    _scene->addRect(0,0,width,height,pen,brush);
    _scene->addLine(width/2,0,width/2,height,pen);
    _scene->addLine(0,height/2,width,height/2,pen);

    QPainterPath p;
    p.moveTo(0,height/2);

    for(int i=0;i<AudioEngine::Instance()->SampledThickLength;i++) {
        float x = i*width/AudioEngine::Instance()->SampledThickLength;
        float y = AudioEngine::Instance()->SampledThick[i];
        y = y*height;
        y += height*0.5;
        p.lineTo(x,y);
    }



    _scene->addPath(p,pen2,QBrush(Qt::transparent));
}
