#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QElapsedTimer>
#include <QGraphicsScene>

#include "oscillatorwidget.h"
#include "mixerwidget.h"
#include "audioengine.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked(bool checked);

private:
    Ui::MainWindow *ui;

    int timerId;
    QElapsedTimer _timer;

    QGraphicsScene *_scene;

protected:
    void timerEvent(QTimerEvent *event);
};

#endif // MAINWINDOW_H
